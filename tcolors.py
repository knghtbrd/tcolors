#!/usr/bin/env python3

# Display the basic 16 colors
print('\x1b[1mStandard 16 colors\x1b[0m')
print('\x1b[30;1m00 \x1b[0;30m', end='')
for color in range(1, 8):
    print('\x1b[48;5;%im%02i ' % (color, color), end='')
print('\x1b[0m\n\x1b[0;30m', end='')
for color in range(8, 16):
    print('\x1b[48;5;%im%02i ' % (color, color), end='')
print('\x1b[0m\n')

# Display 6x6x6 color cube
print('\x1b[1mSliced 6x6x6 color cube\x1b[0m')
for blue in range(0, 6):
    for red in range(0, 3):
        for green in range(0, 6):
            color = 16 + red*36 + green*6 + blue
            print('\x1b[%i;30;48;5;%im%03i ' % (color == 16, color, color), end='')
        print('\x1b[0m \x1b[30m', end='')
    print('')
print('')

for blue in range(0, 6):
    print('\x1b[30m', end='')
    for red in range(3, 6):
        for green in range(0, 6):
            color = 16 + red*36 + green*6 + blue
            print('\x1b[48;5;%im%03i ' % (color, color), end='')
        print('\x1b[0m \x1b[30m', end='')
    print('')
print('')

# Display grayscale ramp
print('\x1b[1m26 shades of gray (reuses 16 and 231)\x1b[0m')
print('\x1b[37;48;5;16m016 ', end='') # provides pure black
for color in range(232, 244):
    print('\x1b[48;5;%im%3i ' % (color, color), end='')
print('\x1b[0m')

print('\x1b[30;1;48;5;231m231 ', end='') # provides pure white
for color in range(255, 243, -1):
    print('\x1b[48;5;%im%3i ' % (color, color), end='')
print('\x1b[0m')
